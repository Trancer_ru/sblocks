;(function () {
	
	'use strict';

	var mobileMenuOutsideClick = function() {

		$(document).click(function (e) {
			var container = $("#colorlib-offcanvas, .js-colorlib-nav-toggle");
			if (!container.is(e.target) && container.has(e.target).length === 0) {

				if ( $('body').hasClass('offcanvas') ) {

					$('body').removeClass('offcanvas');
					$('.js-colorlib-nav-toggle').removeClass('active');

				}
			}
		});

	};

	var offcanvasMenu = function() {

		$('#page').prepend('<div id="colorlib-offcanvas" />');
		$('#page').prepend('<a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle colorlib-nav-white"></a>');
		var clone1 = $('.menu-1 > ul').clone();
		$('#colorlib-offcanvas').append(clone1);
		var clone2 = $('.menu-2 > ul').clone();
		$('#colorlib-offcanvas').append(clone2);

		//var clone3 = $('.animated-img > .social-tags-menu').clone();
		//$('#colorlib-offcanvas').append(clone3);


		$('#colorlib-offcanvas .has-dropdown').addClass('offcanvas-has-dropdown');
		$('#colorlib-offcanvas')
			.find('li')
			.removeClass('has-dropdown');

		// Hover dropdown menu on mobile
		$('.offcanvas-has-dropdown').mouseenter(function(){
			var $this = $(this);

			$this
				.addClass('active')
				.find('ul')
				.slideDown(500, 'easeOutExpo');				
		}).mouseleave(function(){

			var $this = $(this);
			$this
				.removeClass('active')
				.find('ul')
				.slideUp(500, 'easeOutExpo');				
		});


		$(window).resize(function(){

			if ( $('body').hasClass('offcanvas') ) {

    			$('body').removeClass('offcanvas');
    			$('.js-colorlib-nav-toggle').removeClass('active');
				
	    	}
		});
	};

	var burgerMenu = function() {

		$('body').on('click', '.js-colorlib-nav-toggle', function(event){

			if ( $('body').hasClass('overflow offcanvas') ) {
				$('body').removeClass('overflow offcanvas');
			} else {
				$('body').addClass('overflow offcanvas');
			}

			$(this).toggleClass('active');
			event.preventDefault();

		});
	};

	// Loading page
	var loaderPage = function() {
		$(".colorlib-loader").fadeOut("slow");
	};

	var checkPrices = function() {
		return $.ajax({
			type: "get",
			url: URL + "getPrices",
			success: function(data) {
				console.log(data);
				if(data.usdt) $(".usdt-price").html(data.usdt);
				if(data.chr) $(".chr-price").html(data.chr);
			},
			error: function(data) {
			}
		});

	};

	
	$(function(){
		mobileMenuOutsideClick();
		offcanvasMenu();
		burgerMenu();
		loaderPage();
		//checkPrices();
	});
}());

var outsideClick = function(e) {
	if ( $('body').hasClass('offcanvas') ) {
		$('body').removeClass('offcanvas');
		$('.js-colorlib-nav-toggle').removeClass('active');
	}
};

function login(){
	var form =  $(".login-form");
	var inputs = form.find("input");
	var values = {}, errorText = "", resultText = "";

	inputs.each(function() {
		values[this.name] = $(this).val();
	});

	var alertsWrap = $('#loginModal').find('.alerts-wrap');

	if(1 || !values.email || !values.email.length) {
		//errorText = "Email is required";
        errorText = "Email or password is not correct";
		alertsWrap.html('<div class="alert alert-danger"><p>' + errorText + '</p> </div>');
		return;
	} else if(!values.password || !values.password.length) {
		errorText = "Password is required";
		alertsWrap.html('<div class="alert alert-danger"><p>' + errorText + '</p> </div>');
		return;
	} else alertsWrap.html("");

	$(".colorlib-loader").fadeIn("slow");

	//setTimeout(function(){
	return $.ajax({
		type: "post",
		url: URL + "login",
		data: values,
		success: function(data) {
			localStorage.setItem('token', data.accessToken);
			location.href="./private.html";
			$(".colorlib-loader").fadeOut("slow");
		},
		error: function(data) {
			$(".colorlib-loader").fadeOut("slow");
			alertsWrap.html('<div class="alert alert-danger"><p>' + data.responseJSON.resText + '</p></div>');
		}
	});
	//}, 800)
}


function signup(){
	var form =  $(".signup-form");
	var inputs = form.find("input");
	var values = {}, errorText = "", resultText = "";

	inputs.each(function() {
		values[this.name] = $(this).val();
	});

	var alertsWrap = $('#signupModal').find('.alerts-wrap');

	if(!values.email || !values.email.length) {
		errorText = "Email is required";
		alertsWrap.html('<div class="alert alert-danger"><p>' + errorText + '</p> </div>');
		return;
	} else if(!values.password || !values.password.length) {
		errorText = "Password is required";
		alertsWrap.html('<div class="alert alert-danger"><p>' + errorText + '</p> </div>');
		return;
	} else if(values.password !== values.repassword) {
		errorText = "Passwords don't match";
		alertsWrap.html('<div class="alert alert-danger"><p>' + errorText + '</p> </div>');
		return;
	} else alertsWrap.html("");

	$(".colorlib-loader").fadeIn("slow");

	//setTimeout(function(){
	return $.ajax({
		type: "post",
		url: URL + 'signup',
		data: values,
		success: function(data) {
			localStorage.setItem('token', data.accessToken);
			location.href="./private.html";
			$(".colorlib-loader").fadeOut("slow");
			/*
			console.log(data);
			$(".colorlib-loader").fadeOut("slow");
			alertsWrap.html('<div class="alert alert-success"><p>Confirmation link was sent to your email</p></div>');

			 */
		},
		error: function(data) {
			$(".colorlib-loader").fadeOut("slow");
			alertsWrap.html('<div class="alert alert-danger"><p>' + data.responseJSON.resText + '</p></div>');
		}
	});
	//}, 800)
}

function newPassword(){
	var form =  $(".forgot-form");
	var inputs = form.find("input");
	var values = {}, errorText = "", resultText = "";

	inputs.each(function() {
		values[this.name] = $(this).val();
	});

	var alertsWrap = $('#forgotModal').find('.alerts-wrap');

	if(!values.email || !values.email.length) {
		errorText = "Email is required";
		alertsWrap.html('<div class="alert alert-danger"><p>' + errorText + '</p> </div>');
		return;
	} else alertsWrap.html("");

	$(".colorlib-loader").fadeIn("slow");

	//setTimeout(function(){
	return $.ajax({
		type: "post",
		url: URL + 'forgot',
		data: values,
		success: function(data) {
			console.log(data);
			$(".colorlib-loader").fadeOut("slow");
			alertsWrap.html('<div class="alert alert-success"><p>New password was sent to your email</p></div>');
		},
		error: function(data) {
			$(".colorlib-loader").fadeOut("slow");
			alertsWrap.html('<div class="alert alert-danger"><p>' + data.responseJSON.resText + '</p></div>');
		}
	});
	//}, 800)
}

function openForgot(){
	$('#forgotModal').modal('show');
}

$('#loginModal, #signupModal, #forgotModal').on('hidden.bs.modal', function () {
	$(this).find('.alerts-wrap').html("");
});
